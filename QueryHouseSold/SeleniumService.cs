﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text.RegularExpressions;

namespace QueryHouseSold
{
    public partial class SeleniumService : ServiceBase
    {
        public SeleniumService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var timer = new System.Timers.Timer
            {
                Interval = 60 * 60 * 1000 // 1 hour interval
            };
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
            //test();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //private void test()
        {
            var db = new RDDataContext();
            try
            {
                var houseCounter = 0;
                var startTime = DateTime.Now;
                var stopWatch = new Stopwatch();
                stopWatch.Start();
                var linkColl = db.HouseSolds.Select(t => t.Link);
                var url = @"https://www.zoocasa.com/toronto-on-sold-listings";
                //var path = @"D:\HouseSoldTest\ConsoleApp1\ConsoleApp1";
                var path = @"C:\Chrome";
                using (IWebDriver driver = new ChromeDriver(path))
                {
                    var now = DateTime.Now;
                    driver.Navigate().GoToUrl(url);
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                    wait.Until(d => (DateTime.Now - now) - TimeSpan.FromMilliseconds(5000) > TimeSpan.Zero);
                    wait.Until(d => d.FindElements(By.TagName("a")).Where(t => t.Text == "Sign in"));
                    var signinElements = driver.FindElements(By.TagName("a")).Where(t => t.Text == "Sign in");
                    if (signinElements.Count() > 0)
                    {
                        signinElements.First().Click();
                        wait.Until(d => d.FindElements(By.ClassName("sign-in")).Count > 1);
                        var loginForms = driver.FindElements(By.ClassName("sign-in"));
                        if (loginForms.Count > 1)
                        {
                            var textinputElements = loginForms[1].FindElements(By.TagName("text-input"));
                            if (textinputElements != null)
                            {
                                textinputElements[0].FindElement(By.XPath("span/input")).SendKeys("ke@b2bsuppliesusa.com");
                                textinputElements[1].FindElement(By.XPath("span/input")).SendKeys("bloodman123");
                            }
                            loginForms[1].FindElement(By.XPath("div/button")).Click();
                            for (int i = 1; i < 3; i++)
                            {
                                var pageUrl = string.Format(@"https://www.zoocasa.com/toronto-on-sold-listings?page={0}", i);
                                now = DateTime.Now;
                                wait.Until(d => (DateTime.Now - now) - TimeSpan.FromMilliseconds(2000) > TimeSpan.Zero);
                                var cardwrapperColl = driver.FindElement(By.ClassName("page-wrapper")).FindElements(By.XPath("dynamic-listings-page/section/item-group/loading-data/div"));
                                foreach (var card in cardwrapperColl)
                                {
                                    string mlsNumber = string.Empty;
                                    var stopWatchDetail = new Stopwatch();
                                    stopWatchDetail.Start();
                                    var hrefAnchor = card.FindElement(By.TagName("a")).GetAttribute("href");
                                    if (linkColl.Contains(hrefAnchor))
                                    {
                                        stopWatchDetail.Stop();
                                        continue;
                                    }
                                    var imgLink = card.FindElement(By.TagName("img"));
                                    (new OpenQA.Selenium.Interactions.Actions(driver)).Click(imgLink).Perform();
                                    now = DateTime.Now;
                                    wait.Until(d => (DateTime.Now - now) - TimeSpan.FromMilliseconds(10000) > TimeSpan.Zero);
                                    wait.Until(d => d.FindElement(By.ClassName("listing-details")));
                                    try
                                    {
                                        ProcessDetail(driver.PageSource, hrefAnchor, out mlsNumber);
                                        houseCounter++;
                                    }
                                    catch (Exception ex)
                                    {
                                        var mslError = new MLSError
                                        {
                                            MLS = mlsNumber,
                                            Error = ex.ToString(),
                                            Link = hrefAnchor,
                                            ErrorDate = DateTime.Now
                                        };
                                        db.MLSErrors.InsertOnSubmit(mslError);
                                        db.SubmitChanges();
                                    }
                                    var closeIcon = driver.FindElements(By.ClassName("icon-close"));
                                    (new OpenQA.Selenium.Interactions.Actions(driver)).Click(closeIcon[0]).Perform();
                                    now = DateTime.Now;
                                    wait.Until(d => (DateTime.Now - now) - TimeSpan.FromMilliseconds(2000) > TimeSpan.Zero);
                                    stopWatchDetail.Stop();
                                    var tsDetail = stopWatchDetail.Elapsed;
                                    var elapsedTimeDetail = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", tsDetail.Hours, tsDetail.Minutes, tsDetail.Seconds, tsDetail.Milliseconds / 10);
                                }
                                driver.Navigate().GoToUrl(pageUrl);
                            }
                        }
                    }
                }
                stopWatch.Stop();
                var endTime = DateTime.Now;
                var ts = stopWatch.Elapsed;
                var elapsedTime = string.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                var queryLog = new QueryLog
                {
                    ElapseTime = elapsedTime,
                    HouseCount = houseCounter,
                    StartDate = startTime,
                    EndDate = endTime
                };
                db.QueryLogs.InsertOnSubmit(queryLog);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                var mslError = new MLSError
                {
                    MLS = "error",
                    Error = ex.ToString(),
                    Link = "error",
                    ErrorDate = DateTime.Now
                };
                db.MLSErrors.InsertOnSubmit(mslError);
                db.SubmitChanges();
            }
        }

        protected override void OnStop()
        {
        }

        public void SaveImage(string imgUrl, string imgName)
        {
            using (var webClient = new WebClient())
            {
                //webClient.DownloadFileAsync(new Uri(imgUrl), string.Format(@"d:\HouseImages\{0}", imgName));
                webClient.DownloadFileAsync(new Uri(imgUrl), string.Format(@"c:\HouseImages\{0}", imgName));
            }
        }

        public void ProcessDetail(string pageSource, string hrefAnchor, out string mlsNumber)
        {
            mlsNumber = "Empty mls";
            var db = new RDDataContext();
            try
            {
                var htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(pageSource);
                var house = new HouseSold();
                house.CreateDate = DateTime.Now;
                mlsNumber = string.Empty;
                house.Link = hrefAnchor;
                var mainnode = htmlDoc.DocumentNode.SelectSingleNode("//div[@class='listing-details']");
                var streetAddress = mainnode.SelectSingleNode("//span[@itemprop='streetAddress']");
                var addressLocality = mainnode.SelectSingleNode("//span[@itemprop='addressLocality']");
                var addressRegion = mainnode.SelectSingleNode("//span[@itemprop='addressRegion']");
                var postalCode = mainnode.SelectSingleNode("//meta[@itemprop='postalCode']");
                house.Address = streetAddress.InnerText;
                house.Area = addressLocality.InnerText;
                if (addressRegion != null)
                {
                    house.Region = addressRegion.InnerText;
                }
                if (postalCode != null)
                {
                    house.PostalCode = postalCode.InnerText;
                }
                var soldPrice = mainnode.SelectSingleNode("//div[@class='sold-price']/span").InnerText;
                var listPrice = mainnode.SelectSingleNode("//div[@class='list-price']/span[1]").InnerText;
                var soldDate = mainnode.SelectSingleNode("//div[@class='list-price']/span[2]").InnerText;
                if (soldDate.ToLower().Contains("day"))
                {
                    int.TryParse(Regex.Match(soldDate, @"\d+").Value, out int iDay);
                    house.SoldDate = DateTime.Now.AddDays(iDay == 0 ? -1 : -iDay);
                }
                if (soldDate.ToLower().Contains("hour"))
                {
                    house.SoldDate = DateTime.Now.AddHours(-int.Parse(Regex.Match(soldDate, @"\d+").Value));
                }
                house.SoldPrice = soldPrice;
                house.AskPrice = listPrice;
                var description = mainnode.SelectSingleNode("//p[@itemprop='description']").InnerText;
                house.Description = description;
                var badsbathsColl = mainnode.SelectNodes("//div[@class='beds-baths']/span");
                house.Bedroom = badsbathsColl[0].InnerText;
                house.Bathroom = badsbathsColl[1].InnerText;
                house.Size = badsbathsColl[2].InnerText;
                house.Park = badsbathsColl[3].InnerText;
                var propertydetails = mainnode.SelectNodes("//property-details/details-table");
                foreach (var detail in propertydetails)
                {
                    var title = detail.SelectSingleNode("section/header/span").InnerText;
                    var divColl = detail.SelectNodes("section/div");
                    if (divColl != null)
                    {
                        if (title != "Room Layout" && title != "Extras")
                        {
                            foreach (var div in divColl)
                            {
                                var propertyDetail = new PropertyDetail();
                                var name = div.SelectSingleNode("span[1]").InnerText;
                                var value = div.SelectSingleNode("span[2]").InnerText;
                                if (name.ToLower().Contains("mls"))
                                {
                                    mlsNumber = value;
                                    house.MLS = mlsNumber;
                                    db.HouseSolds.InsertOnSubmit(house);
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    propertyDetail.MLS = mlsNumber;
                                    propertyDetail.Name = name;
                                    propertyDetail.Value = value;
                                    propertyDetail.Title = title;
                                    db.PropertyDetails.InsertOnSubmit(propertyDetail);
                                    db.SubmitChanges();
                                }
                            }
                        }
                        if (title == "Room Layout")
                        {
                            foreach (var div in divColl)
                            {
                                var roomName = div.SelectSingleNode("span[1]").InnerText;
                                var roomLevel = div.SelectSingleNode("span[2]").InnerText;
                                var roomSize = div.SelectSingleNode("span[3]").InnerText;
                                var roomLayout = new RoomLayout
                                {
                                    MLS = mlsNumber,
                                    RoomName = roomName,
                                    Level = roomLevel,
                                    Size = roomSize
                                };
                                db.RoomLayouts.InsertOnSubmit(roomLayout);
                                db.SubmitChanges();
                            }
                        }
                        if (title == "Extras")
                        {
                            var extras = divColl[0].SelectSingleNode("p").InnerText;
                            house.Extras = extras;
                        }
                    }
                }
                db.SubmitChanges();

                var priceHistoryColl = mainnode.SelectNodes("//price-history/loading-data/div[@class='table']/div");
                if (priceHistoryColl != null && priceHistoryColl.Count > 0)
                {

                    for (int i = 1; i < priceHistoryColl.Count; i++)
                    {
                        var detailSpans = priceHistoryColl[i].SelectNodes("span");
                        var priceHistory = new PriceHistory
                        {
                            MLS = mlsNumber,
                            ListPrice = detailSpans[1].InnerText,
                            Type = detailSpans[2].InnerText,
                            Status = detailSpans[3].InnerText,
                            Date = DateTime.ParseExact(detailSpans[4].InnerText, "d/M/yyyy", CultureInfo.InvariantCulture),
                            SoldPrice = detailSpans[5].InnerText
                        };
                        db.PriceHistories.InsertOnSubmit(priceHistory);
                        db.SubmitChanges();
                    }
                }

                var schoolColl = mainnode.SelectNodes("//nearby-schools/loading-data/div[@class='school-cell']");
                if (schoolColl != null && schoolColl.Count > 0)
                {
                    for (int i = 0; i < schoolColl.Count; i++)
                    {
                        var name = schoolColl[i].SelectSingleNode("div/a").InnerText;
                        var link = schoolColl[i].SelectSingleNode("div/a").Attributes["href"].Value;
                        var details = string.Join(", ", schoolColl[i].SelectNodes("div[1]/span").Select(t => t.InnerText));
                        double rating;
                        double.TryParse(schoolColl[i].SelectSingleNode("div[2]/span[1]").InnerText, out rating);
                        var school = new NearbySchool
                        {
                            MLS = mlsNumber,
                            Name = name,
                            Rating = rating,
                            Detail = details
                        };
                        db.NearbySchools.InsertOnSubmit(school);
                        db.SubmitChanges();
                    }
                }

                var imgColl = mainnode.SelectNodes("//div[@class='carousel-thumbnails']/ul/li/image-loader/img");
                if (imgColl != null)
                {
                    foreach (var img in imgColl)
                    {
                        var url = img.Attributes["src"].Value;
                        var regPattern = @"[\w-]+\.jpg";
                        var imgName = Regex.Match(url, regPattern).Value;
                        var image = new HouseImage
                        {
                            Name = imgName,
                            Link = url,
                            MLS = mlsNumber
                        };
                        db.HouseImages.InsertOnSubmit(image);
                        db.SubmitChanges();
                        SaveImage(url, imgName);
                    }
                }
            }
            catch (Exception ex)
            {
                var mslError = new MLSError
                {
                    MLS = mlsNumber + " detail",
                    Error = ex.ToString(),
                    Link = "error",
                    ErrorDate = DateTime.Now
                };
                db.MLSErrors.InsertOnSubmit(mslError);
                db.SubmitChanges();
            }
        }
    }
}
