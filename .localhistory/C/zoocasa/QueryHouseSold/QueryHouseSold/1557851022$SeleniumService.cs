﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace QueryHouseSold
{
    public partial class SeleniumService : ServiceBase
    {
        public SeleniumService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var timer = new System.Timers.Timer();
            timer.Interval = 60 * 60 * 1000; // 1 hour interval
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected override void OnStop()
        {
        }
    }
}
