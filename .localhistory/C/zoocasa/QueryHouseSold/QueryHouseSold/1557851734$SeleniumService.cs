﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;

namespace QueryHouseSold
{
    public partial class SeleniumService : ServiceBase
    {
        public SeleniumService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var timer = new System.Timers.Timer();
            timer.Interval = 60 * 60 * 1000; // 1 hour interval
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var db = new HouseSoldEntities();
            var linkColl = db.HouseSolds.Select(t => t.Link);
            var url = @"https://www.zoocasa.com/toronto-on-sold-listings";
            using (IWebDriver driver = new ChromeDriver())
            {
                var now = DateTime.Now;
                driver.Navigate().GoToUrl(url);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                wait.Until(d => (DateTime.Now - now) - TimeSpan.FromMilliseconds(5000) > TimeSpan.Zero);
                wait.Until(d => d.FindElements(By.TagName("a")).Where(t => t.Text == "Sign in"));
                var signinElements = driver.FindElements(By.TagName("a")).Where(t => t.Text == "Sign in");
            }
        }

        protected override void OnStop()
        {
        }
    }
}
